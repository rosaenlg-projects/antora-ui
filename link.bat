@echo off
if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)

cd c:\rosaenlg\antora-ui
mklink /D node_modules\rosaenlg c:\rosaenlg\rosaenlg\packages\rosaenlg

pause
